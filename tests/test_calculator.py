"""Тестування калькулятора."""
from lab2023.calculator import calculator


def test_sum():
    """Тестування додавання."""
    res = calculator.sum(2, 2)
    assert res == 4


def test_sub():
    """Тестування віднімання."""
    res = calculator.sub(6, 4)
    assert res == 2


def test_mult():
    """Тестування множення."""
    res = calculator.mult(4, 6)
    assert res == 24


def test_div():
    """Тестування ділення."""
    res = calculator.div(6, 4)
    assert res == 1.5
