"""Тестування фабрики калькулятора."""
from lab2023.calculator_factory import calculator_factory as factory
import unittest.mock


def test_interaction():
    """Тестування взаємодії з користувачем."""
    with unittest.mock.patch('builtins.input', return_value=1):
        res = factory.interaction()
        assert res == 1


def test_process_operation():
    """Тестування отримання функції."""
    assert factory.process_operation("1").__name__ == 'sum'
    assert factory.process_operation("2").__name__ == 'sub'
