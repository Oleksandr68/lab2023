"""Калькулятор."""


def sum(a, b):
    """Додавання."""
    return a + b


def sub(a, b):
    """Віднімання."""
    return a - b


def mult(a, b):
    """Множення."""
    return a * b


def div(a, b):
    """Ділення."""
    return a / b


all_actions = [sum, sub, mult, div]
