"""Конфігурація."""


import configparser
import os


class Configuration:
    """Конфігурація."""

    FILE_NAME = "config.ini"

    def __init__(self):
        """Ініціалізація."""
        dir = os.path.abspath(os.curdir)
        filename = os.path.join(dir, "lab2023", self.FILE_NAME)
        self.config = configparser.ConfigParser()
        self.config.sections()
        self.config.read(filename)

    @property
    def read_connection_string(self):
        """Читання стрічки підключення."""
        return self.config["DB"]["ConnectionString"]
