"""Точка входу."""
import uvicorn
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from fastapi import status
from history_provider.historyrepository import HistoryRepository
from history_provider.operationrepository import OperationRepository
from calculator_factory import calculator_factory


class CalculateItem(BaseModel):
    """Сутність оперції для POST методу."""

    operation_id: str
    input_a: float
    input_b: float


app = FastAPI()
history_repository = HistoryRepository()
operation_repository = OperationRepository()


@app.get("/")
def read_root():
    """Вхідна точка калькулятора."""
    return "Колькулятор"


@app.get("/operations")
def read_all_operations():
    """Читання всіх операцій."""
    res = operation_repository.read_operations()
    return res


@app.get("/history")
def read_all_history():
    """Читання історії."""
    res = history_repository.read_history()
    return res


@app.get("/history/{id}")
def read_history(id):
    """Читання історії по ідентифікатору."""
    res = history_repository.read_history_by_id(id)
    if res is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    else:
        return res


@app.post("/calculate")
def read_item(item: CalculateItem):
    """Виконання операції розрахунку."""
    try:
        function = calculator_factory.process_operation(item.operation_id)
        result = function(item.input_a, item.input_b)
        data = []
        data.append(function.__doc__)
        data.append(item.input_a)
        data.append(item.input_b)
        data.append(result)
        history_repository.add_to_history(data)
        return {"Result": result}
    except Exception as ex:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(ex))


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
