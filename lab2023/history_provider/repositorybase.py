"""Базовий репозиторій."""


from sqlalchemy import *
from sqlalchemy_utils import database_exists, create_database
from history_provider.entities import Base
from configuration.configuration import Configuration


class RepositoryBase:
    """Базовий репозиторій."""

    def __init__(self):
        """Базовий репозиторій."""
        self.cfg = Configuration()
        conn_str = self.cfg.read_connection_string
        self.engine = create_engine(conn_str)

        if not database_exists(self.engine.url):
            create_database(self.engine.url)
        Base.metadata.create_all(self.engine)
