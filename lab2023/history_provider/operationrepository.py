"""Репозиторій операцій."""


from sqlalchemy import *
from sqlalchemy.orm import Session
from calculator import calculator
from history_provider.entities import Operation
from history_provider.repositorybase import RepositoryBase


class OperationRepository(RepositoryBase):
    """Репозиторій операцій."""

    def __init__(self):
        """Репозиторій операцій."""
        super().__init__()
        with Session(self.engine) as session:
            for operation in calculator.all_actions:
                res = session.query(Operation).where(Operation.operation_name == operation.__doc__).count()
                if res == 0:
                    stmt = insert(Operation).values(operation_name=operation.__doc__)
                    session.execute(stmt)
            session.commit()

    def read_operations(self):
        """Читання всіх операцій."""
        with Session(self.engine) as session:
            return session.query(Operation).all()
