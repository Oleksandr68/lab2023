"""Сутності БД."""


from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
Base = declarative_base()


class Historу(Base):
    """Клас сутності історія."""

    __tablename__ = "history"

    id = Column(Integer, primary_key=True)
    operation_id = Column(Integer, ForeignKey('operation.id'))
    operation = relationship("Operation", back_populates="history")
    input_a = Column(Float)
    input_b = Column(Float)
    result = Column(Float)


class Operation(Base):
    """Клас сутності операція."""

    __tablename__ = "operation"

    id = Column(Integer, primary_key=True)
    operation_name = Column(String, unique=True)
    history = relationship("Historу", back_populates="operation")
