"""Провайдер даних."""


from sqlalchemy import *
from sqlalchemy.orm import Session, joinedload
from history_provider.entities import Historу
from history_provider.operationrepository import Operation
from history_provider.repositorybase import RepositoryBase


class HistoryRepository(RepositoryBase):
    """Провайдер даних."""

    def __init__(self):
        """Конструктор."""
        super().__init__()

    def read_history(self):
        """Читання історії."""
        with Session(self.engine) as session:
            result = session.query(Historу).options(joinedload(Historу.operation)).all()
            return result

    def read_history_by_id(self, id):
        """Читання історії по id."""
        with Session(self.engine) as session:
            result = session.query(Historу).options(joinedload(Historу.operation)).filter(id is None or Historу.id == id).first()
            return result

    def add_to_history(self, messages):
        """Запис в історію."""
        with Session(self.engine) as session:
            res = session.query(Operation).where(Operation.operation_name == messages[0]).one()
            stmt = (
                insert(Historу).values(
                    operation_id=res.id,
                    input_a=messages[1],
                    input_b=messages[2],
                    result=messages[3]
                )
            )
            session.execute(stmt)
            session.commit()
