"""Фабрика калькулятора."""
from calculator import calculator


def interaction():
    """Вибір дії."""
    print("Виберіть дію:")
    print("1: додавання\r\n2: віднімання\r\n3: множення\r\n4: ділення")
    operation = input(">")
    return operation


def process_operation(operation):
    """
    Функція для вибору методя з кальтулатора.

    :returns: метод, яким будуть поводитись розрахунки
    """
    match operation:  # match з'явився в python 3.10
        case "1":
            return calculator.sum
        case "2":
            return calculator.sub
        case "3":
            return calculator.mult
        case "4":
            return calculator.div
        case _:
            raise Exception("Неправильна операція")
