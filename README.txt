python -m venv venv
.\venv\Scripts\activate
deactivate

pip install -r .\requirements.txt
tox -e py311,flake8

[Get Started with Docker](https://www.docker.com)

Запуск Docker
> docker-compose up

http://localhost:8000/docs