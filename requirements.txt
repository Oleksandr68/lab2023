tox
pytest
flake8
sqlalchemy
psycopg2
sqlalchemy-utils
fastapi
uvicorn[standard]